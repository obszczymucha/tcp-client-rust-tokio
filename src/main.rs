use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use std::env;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <message>", args[0]);
        std::process::exit(1);
    }

    let message = &args[1];
    let mut stream = TcpStream::connect("172.25.80.1:8080").await?;

    stream.write_all(message.as_bytes()).await?;

    Ok(())
}
